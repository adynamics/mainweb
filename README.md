# ¿Cómo publicar en la web principal de aDynamics?

## Configuración del entorno de publicación

Para públicar en la web de aDynamics hacen falta pocas herramientas:
* Tu editor favorito para escribir en formato [Markdown](https://es.wikipedia.org/wiki/Markdown).
* El cliente de Git, la herramienta para control de versiones.
* La herramienta para la generación de web estáticas, [Hugo](https://gohugo.io/).

Hemos elegido Markdown para este wiki y para publicar en la web porque es uno de los formatos más sencillos de entender para las personas y es fácil de procesar por las máquinas. Como editor puedes usar uno cualquiera que guarde en [ASCII](https://es.wikipedia.org/wiki/ASCII) el fichero, por ejemplo no se pueden editar documentos con Microsoft Office. Si no estas seguro de cual elegir te podemos recomendar [Visual Studio Code](https://code.visualstudio.com).

Lo siguiente que necesitas es un cliente Git. Usamos Git para poder tener la web versioneada y volver a cualquier punto de su estado si así lo quisiéramos. Si usas Windows te puedes descargar el cliente de Git desde [esta página](https://git-scm.com/downloads). Si usas GNU/Linux lo puedes instalar desde tu sistema de paquetes: apt install git, yum install git, etc...

La primera vez que se quiere usar Git hay que configurar al menos tu nombre y tu correo que es lo que aparecerá en los logs de cambios en el código, en este caso los artículos en formato Markdown. Los comandos serían los siguientes sustituyendolos por los tuyos.

```text
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

## ¿Cómo escribir y publicar en la web?

Nosotros trabajaremos principalmente en los directorios content/es (para contenido en castellano) y content/en (para contenido en inglés) y para crear una nueva entrada ejecutaremos el comando:

```text
➜  mainweb git:(master) hugo new post/mi-primer-post.md
```

A continuación abre ese fichero que se ha creado y veras que contiene en la parte superior una cabecera con metadatos y espacio debajo para escribir. Para que veas el formato de un documento completo te recomiendo que mires [este código en formato Markdown](https://gitlab.com/adynamics/mainweb/blob/master/content/es/post/google.md).

Una vez hayas terminado tu artículo deberas hacer un commit y hacer push al repositorio en tu espacio. A continuación harás un pull request al repositorio original para que alguien con permisos de despliegue revise el documento y lo suba al servidor.

Básicamente este es el flujo de edición. Si crees que podríamos ampliar alguna parte de este documento háznoslo saber, lo aceptaremos con gusto.