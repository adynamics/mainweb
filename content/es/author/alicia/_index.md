+++
# Display name
name = "Alicia Ortega Muñoz"

# Username (this should match the folder name)
authors = ["alicia"]

# Is this the primary user of the site?
superuser = false

# Role/position
role = "Gestora de Proyectos"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "Areas de interes gestión de proyectos, metodologías ágiles, SCRUM, KANBAN y hardware libre."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "aliciaortega1986@gmail.com"

# List (academic) interests or hobbies
interests = [
  "Gestión de Proyectos",
  "SCRUM",
  "Hardware Libre"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo"]

+++
