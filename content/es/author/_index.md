
+++
# Display name
name = "Jordi López Amat"

# Username (this should match the folder name)
authors = ["jordila"]


# Role/position
role = "Developer"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "Areas of interest Unified Communications and federated networks."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "jordila@librebits.info"

# List (academic) interests or hobbies
interests = [
  "Unified Communications",
  "Federated networks",
  "IPv6",
  "HTML5"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Team"]

+++
