+++
# Display name
name = "Antonio Pardo"

# Username (this should match the folder name)
authors = ["apardo"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Arquitecto de Sistemas"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "Areas de interes comunicaciones unificadas y el lenguaje Elixir."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "apardo@aps.systems"

# List (academic) interests or hobbies
interests = [
  "Comunicaciones Unificadas",
  "Elixir",
  "Ruby",
  "Gobierno Abierto"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo"]

+++
