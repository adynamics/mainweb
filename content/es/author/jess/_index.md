+++
# Display name
name = "Jess Suárez"

# Username (this should match the folder name)
authors = ["jess"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Técnica Informática"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "Areas de interes sistemas operativos."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "jess@disroot.org"

# List (academic) interests or hobbies
interests = [
  "GNU/Linux",
  "Software Libre",
  "Hardware",
  "Virtualización"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Equipo"]

+++
