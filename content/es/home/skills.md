+++
# Feature/Skill widget.
widget = "featurette"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Habilidades"
subtitle = ""

# Order that this section will appear in.
weight = 30

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "arrows-alt"
  icon_pack = "fas"
  name = "devops"
  description = "99%"
  
[[feature]]
  icon = "phone"
  icon_pack = "fas"
  name = "Telefonía IP"
  description = "80%"  
  
[[feature]]
  icon = "diagnoses"
  icon_pack = "fas"
  name = "eVoting"
  description = "60%"

[[feature]]
  icon = "dharmachakra"
  icon_pack = "fas"
  name = "ARM"
  description = "90%"

[[feature]]
  icon = "bezier-curve"
  icon_pack = "fas"
  name = "Inteligencia Artificial"
  description = "65%"

[[feature]]
  icon = "mobile"
  icon_pack = "fas"
  name = "Android"
  description = "70%"

+++
