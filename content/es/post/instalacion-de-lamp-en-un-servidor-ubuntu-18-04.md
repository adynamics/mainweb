+++
title = "Instalacion de LAMP en un Servidor Ubuntu 18.04"
date = 2019-05-15T07:50:15+02:00
draft = false

authors = ["jess"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['sql', 'lamp', 'php']
categories = ['main', 'dessarrollo', 'php']

[header]
image = "https://source.unsplash.com/collection/765594/1600x350"

+++

Un servidor LAMP (Linux-Apache-MySQL-PHP) es un conjunto de sistemas, lenguajes o tecnologías que se utilizan muy habitualmente de manera conjunta para alcanzar una solución global, como configurar sitios web o servidores dinámicos de forma sencilla. 

Para instalar LAMP en Ubuntu 18.04 es necesario ejecutar los siguientes comandos

```bash
sudo su -
apt update
apt install apache2
```

Comprobamos que la instalación es correcta introduciendo la IP del servidor en el navegador

![Figura 1](/img/cap1.png)

Instalamos PHP, la librería que lo conecta con Apache y la librería que lo conecta con la base de datos MySQL.

```bash
apt install php
apt install libapache2-mod-php
apt install php-mysql
```

Reiniciamos Apache para que carguen los módulos instalados

```bash
service apache2 restart
```

Creamos el archivo php.info 

```bash
nano /var/www/html/phpinfo.php
```

Con el siguiente contenido

![Figura2](/img/cap2.png)

Para comprobar que es correcto [cargamos la página en el navegador](http://ip-servidor/phpinfo.php)

![Figura3](/img/cap3.png)

A continuación instalamos MySQL

```bash
apt install mysql-server
```

Introduciremos una contraseña para el usuario root de MySQL y la confirmaremos, hay que tener en cuenta que este usuario root no es el del sistema operativo ya que MySQL cuenta con sus propios usuarios.

![Figura4](/img/cap4.png)

![Figura5](/img/cap5.png)

Nos conectamos a la base de datos para comprobar que funciona

```bash
mysql -u root -p
```

![Figura6](/img/cap6.png)

Finalmente instalamos phpMyAdmin

```bash
apt install phpmyadmin
```

Durante la instalación se nos pedirá elegir el servidor web, con el Apache que trabajamos.

![Figura7](/img/cap7.png)

Tendremos que introducir la contraseña root de MySQL anteriormente generada.

![Figura8](/img/cap8.png)

![Figura9](/img/cap9.png)

Y ahora tendremos que asignarle una contraseña al usuario llamado “phpmyadmin”, que es el que usa la aplicación web para interactuar con la base de datos.

![Figura10](/img/cap10.png)

Comprobamos que la instalación [es correcta en el navegador](http://ip_server/phpmyadmin)

![Figura11](/img/cap11.png)

Se inicia sesión

![Figura12](/img/cap12.png)


