+++
title = "Filosofia Del Software Libre"
date = 2019-04-08T14:17:48+02:00
draft = false

authors = ["jess"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['systems', 'free software', 'linux', 'stallman']
categories = ['main', 'free software']

[header]
image = "https://source.unsplash.com/collection/765594/1600x350"

+++

El Software Libre o Free Software es aquel que respeta la libertad de sus usuarios y la comunidad.
Se considera libre todo el software que permita a sus usuarios la libertad de ejecutarlo, copiarlo, distribuirlo, estudiarlo, modificarlo y mejorarlo, para ello es imprescindible tener acceso al código fuente. 

La filosofía del Software Libre se basa en cuatro libertades esenciales:

  * **Libertad 0:** La libertad de ejecutar el programa como se desee, con cualquier propósito. 
  * **Libertad 1:** La libertad de estudiar cómo funciona el programa, y cambiarlo para que haga lo que usted quiera.
  * **Libertad 2:** La libertad de redistribuir copias para ayudar a otros
  * **Libertad 3:** La libertad de distribuir copias de sus versiones modificadas a terceros. Esto le permite ofrecer a toda la comunidad la oportunidad de beneficiarse de las modificaciones.

Si un programa no cumple con estas cuatro libertades no se puede considerar libre sino de código abierto u Open Source, cuya filosía es diferente. 

El Software Libre surgió en los principios de los años 80 de la mano del Proyecto GNU, desarrollado por Richard Stallman para desarrollar un sistema operativo libre por completo. Poco después Stallman acuñó el término y **fundó la Free Software Foundation** para promover el concepto de software libre, **en 1986 publicó [el manifiesto GNU](https://www.gnu.org/gnu/manifesto.es.html)**  y **en 1989 la primera versión de la [licencia GPL](https://www.gnu.org/licenses/licenses.es.html) (General Public License)** la cual ha ido evolucionando con el paso del tiempo y derivado en varios tipos de licencia.

El Software Libre no es una cuestión de precio, lo es de libertad. Existe la creencia errónea de que este tipo de software es gratuito debido a que la palabra Free traducida del inglés al español significa también gratis. También existe la creencia de que vender programas libres no concuerda con el espíritu del proyecto GNU, desde el propio proyecto nos aseguran que esto es un malentendido y recomiendan a todas las personas que distribuyan este software que [cobren tanto como deseen o sea posible](https://www.gnu.org/philosophy/selling.html).

En resumen, la filosofía del Software Libre consiste en compartir, en no privar a nadie a tener acceso al uso de herramientas informáticas y en evitar el monopolio del desarrollo de software solo a unas cuantas grandes empresas.
