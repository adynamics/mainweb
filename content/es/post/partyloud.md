+++
title = "Partyloud"
date = 2019-04-22T17:52:33+02:00
draft = false

authors = ["radikal"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['utilidades','terminal','partyloud','fingerprinting']
categories = ['main','terminal','utilidades']

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[header] 
image = "https://source.unsplash.com/collection/765594/1600x300"

+++

[PartyLoud][link1] es un proyecto muy interesante.
Se trata de una herramienta para crear tráfico falso con el fin de mitigar el rastreo en las redes locales.
La idea detrás de este script es que si haces mucho ruido en la red (en forma de peticiones http) será menos fácil para un atacante rastrear tu navegación real.

![PartyLoud][imagen1]

Este proyecto está basado en [noisy.py][link2]

## Características

- Lista de urls configurables (partyloud.conf) y lista de bloqueos (badwords).
- Motor de petición multihilo (el nómero de hilos es igual al número de urls en partyloud.conf).
- Mecanismo de recuperación de errores para proteger los motores de fallos.
- Los agentes de usuario falsificados evitan la técnica de [*fingerprinting*][link3] (cada motor tiene un agente de usuario diferente).
- Interfaz dinámica.

## Instalación

Para instalar y usar PartyLoud, necesitaremos git y curl.

En Debian y derivadas:

`sudo apt install git curl`

Clonamos su repositorio en nuestro disco:

`git clone https://github.com/realtho/PartyLoud.git`

Entramos en el directorio recién clonado:

`cd PartyLoud`

Le damos permisos de ejecución al script:

`chmod +x partyloud.sh`

Y ya lo podemos ejecutar con:

`./partyloud.sh`

Para finalizarlo, puedes darle a la tecla **INTRO** o **CTRL + c**.

Para ver más opciones, ver la ayuda:

`./partyloud.sh -h`

[link1]: https://github.com/realtho/PartyLoud "PartyLoud on GitHub"
[link2]: https://github.com/1tayH/noisy "Noisy on GitHub"
[link3]: https://en.wikipedia.org/wiki/Fingerprint_(computing)
[imagen1]: https://camo.githubusercontent.com/bb1732abcaf93b1d8ba77dbadc6f11aa1ffd42c4/68747470733a2f2f692e696d6775722e636f6d2f636e31654546732e706e67

