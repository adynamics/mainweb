+++
title = "Cómo instalar FreeSwitch"
date = 2019-04-20T22:30:15+01:00
draft = false

authors = ["apardo"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['sip', 'telephony', 'freeswitch', 'pbx']
categories = ['main', 'telephony']

[header]
image = "https://source.unsplash.com/collection/765594/1600x350"

+++

En este artículo veremos como instalar [FreeSwitch](https://freeswitch.com), una PBX más versátil que [Asterisk](https://www.asterisk.org), para montar un servidor del protocolo [SIP](https://en.wikipedia.org/wiki/Session_Initiation_Protocol). La instalación la haremos sobre Ubuntu 18.04 LTS, en otras distribuciones no debería ser muy diferente si se sabe traducir los comandos de una a otra distribución.

# Prerequisitos

Se aconseja partir de un servidor recien desplegado, o máquina virtual, y los paquetes para poder compilar FreeSwitch son los siguientes:

```bash
$ apt-get install --yes build-essential pkg-config uuid-dev \
zlib1g-dev libjpeg-dev libsqlite3-dev libcurl4-openssl-dev \
libpcre3-dev libspeexdsp-dev libldns-dev libedit-dev libtiff5-dev \
yasm libopus-dev libsndfile1-dev liblua5.3-dev
```

A continuación es necesario descargar el código fuente de FreeSwitch desde *https://files.freeswitch.org/freeswitch-releases/* Nosotros hemos usado la versión 1.8.5. Así que nos iremos al directorio */usr/local/src* y como root descargaremos esa versión.

```bash
$ wget https://files.freeswitch.org/freeswitch-releases/freeswitch-1.8.5.tar.gz
$ tar xvzf freeswitch-1.8.5.tar.gz
$ cd freeswitch-1.8.5
```

Ahora ya podemos compilar FreeSwitch:

```bash
./configure && make
```

Algunos errores se pueden presentar mientras se compila por la falta de algunos paquetes, los instalamos y reiniciamos la compilación:

```bash
./configure && make clean && make
```

Tras una compilación exitosa instalaremos el software. La localización por defecto es */usr/local/freeswitch*:

```bash
make install
```

# Configurar FreeSwitch como servicio

Crearemos el fichero */etc/systemd/system/freeswitch.service* con el editor que más nos guste y añadiremos lo siguiente:

```bash
; This file in installations built from Master can be found in
; /usr/src/freeswitch.git/debian
; or
; /usr/src/freeswitch/debian
[Unit]
Description=freeswitch
After=syslog.target network.target local-fs.target

[Service]
; service
Type=forking
PIDFile=/usr/local/freeswitch/run/freeswitch.pid
PermissionsStartOnly=true
; blank ExecStart= line flushes the list
ExecStart=
ExecStart=/usr/local/freeswitch/bin/freeswitch -u freeswitch -g freeswitch -ncwait -nonat -rp
TimeoutSec=45s
Restart=on-failure
; exec
WorkingDirectory=/usr/local/freeswitch/bin
User=root
Group=daemon
LimitCORE=infinity
LimitNOFILE=100000
LimitNPROC=60000
;LimitSTACK=240
LimitRTPRIO=infinity
LimitRTTIME=7000000
IOSchedulingClass=realtime
IOSchedulingPriority=2
CPUSchedulingPolicy=rr
CPUSchedulingPriority=89
UMask=0007

[Install]
WantedBy=multi-user.target
```

Iniciamos el servicio:

```bash
service freeswitch start
```

Y por último ya podemos ingresar en la consola con el comando */usr/local/freeswitch/bin/fs_cli*

Recordar que antes de exponer el servicio a Internet es casi obligatorio leerse [la documentación](https://freeswitch.org/confluence/display/FREESWITCH/FreeSWITCH+First+Steps) disponible en el wiki.

Otro día veremos como hacer un teléfono SIP para la web usando alguna librería de SIP sobre Websockets.