+++
title = "Don't Be Evil"
date = 2019-03-17T19:25:00+01:00
draft = false

authors = ["pietre"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['google', 'sistemas']
categories = ['main']

[header]
image = "https://source.unsplash.com/collection/765594/1600x350"

+++

Google, desde aproximadamente el año 2000 ha monopolizado las búsquedas de Internet a nivel mundial, después de su éxito con el buscador quería más, por eso compró Android, un sistema operativo basado en GNU/Linux, no contentos con eso Google desplegó una serie de servicios de espionaje en sus móviles Android según ellos para facilitar la vida a sus usuarios, empezando por tus búsquedas personales, tus chats, tus comentarios anónimos en foros.

No contentos con eso, actualmente en el año 2018 monitorizan de forma sistematizada todo los aspectos de la vida de la personas, donde están, donde trabajan, donde van a comer, cuales son sus gustos musicales.

Es por ello, que desde la total indignidad creamos este espacio virtual para resistir, para embarcarnos en un proyecto que seguirá mas allá de sus creadores, un proyecto que su única meta es cambiar el sistema con el que operan por defecto estos dispositivos de control de Google.

Si, el mundo ha cambiado hacia una sociedad conectada, conectada a un centro de control que se llama Google y eso ya no lo podemos permitir, por que nadie, repito NADIE, puede tener todo el control de toda la información de todas las personas, eso se llama monopolio, y el más atroz es que controla nuestros gustos, sentimientos y emociones.