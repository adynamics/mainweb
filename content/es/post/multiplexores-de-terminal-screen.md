+++
title = "Multiplexores De Terminal - Screen"
date = 2019-04-10T14:01:12+02:00
draft = false

authors = ["radikal"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['terminal','utilidades','screen']
categories = ['main','terminal','utilidades']

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[header] 
image ="https://source.unsplash.com/collection/765594/1600x350"

+++

Hoy vamos a hablar de los multiplexores de terminal, una herramienta muy útil con muchas utilidades.

# ¿Que te permite hacer un multiplexor?

Un **multiplexor** te permite:

- Usar múltiples ventanas de shell desde una conexión ssh.
- Mantener la shell activa incluso con cortes en la conexión de red.
- Desconectar y reconectar a una sesión de shell desde múltiples ubicaciones.
- Ejecutar procesos duraderos sin tener un terminal abierto.

Tiene muchas utilidades. Uno de los multiplexores más conocidos (del que hablaremos hoy) es [screen][screen]. Otro día hablaremos de otros multiplexores como [tmux][tmux] y [byobu][byobu].

# screen

Instalar screen en [Debian](http://debian.org/) y derivadas:

`sudo apt install screen`

Al ejecutar screen es muy útil darle un **nombre de sesión** para poder reconocerla y acceder más fácilmente.

`$ screen -S $nombre_de_sesión`

Ver sesiones activas:

`$ screen -ls`

Conectar a una sesión:

`$ screen -r $nombre_de_sesión`

# Comandos

Usa "`ctrl`+`a`" como comando principal seguido de una tecla.

Ejemplos:

- "`ctrl`+`a`" `c`  - Crea una nueva ventana
- "`ctrl`+`a`" `n`  - Va a la siguiente ventana
- "`ctrl`+`a`" `p`  - Va a la ventana previa
- "`ctrl`+`a`" `d`  - Desconectar de la sesión (los procesos siguen ejecutándose)
- "`ctrl`+`a`" `S`  - Divide la ventana horizontalmente
- "`ctrl`+`a`" `|`  - Divide la ventana verticalmente

# Configuración

Su archivo de configuración global está en *`/etc/screenrc`* y el de usuario en *`~/.screenrc`*

Configuración de ejemplo:

~~~

# screenrc by radikal
# Turn off the annoying startup message

startup_message off
defscrollback 10000
hardcopy_append on
defutf8 on
term xterm
termcapinfo xterm*|Eterm|mlterm|rxvt 'hs:ts=\E]0;:fs=\007:ds=\E]0;screen\007'

# 8-16 Color Support

termcap  xterm AF=\E[3%dm
terminfo xterm AF=\E[3%p1%dm
termcap  xterm AB=\E[4%dm
terminfo xterm AB=\E[4%p1%dm

# 256 Color Support

terminfo xterm Co=256
termcap  xterm Co=256
termcap  xterm AF=\E[38;5;%dm
terminfo xterm AF=\E[38;5;%p1%dm
termcap  xterm AB=\E[48;5;%dm
terminfo xterm AB=\E[48;5;%p1%dm

# Set status line in window title, preview:
# [hostname]         0$ top  3-$ watch  (4*$ bash)        [load: 0,01 0,01 0,00] [mi�, 10/04/2019] [14:59:00]

hardstatus alwayslastline '%{= kG}[%{G}%H%? %1`%?%{g}] [%= %{= kw}%-w%{+b yk} %n*%t%?(%u)%? %{-}%+w %=%{g}] [%l] %{B}[%D, %d/%m/%Y] %{W}[%c:%s]%{g}'

# use F7-F8 to turn on|off the status bar off at the bottom of the screen
bindkey -k k7 hardstatus alwayslastline
bindkey -k k8 hardstatus alwaysignore

# Set left and right meta key mods ALT-nm.

bindkey "^[n" prev
bindkey "^[m" next

# Screens by default

~~~

Para más información ver su página de manual: `man screen`

[screen]: https://savannah.gnu.org/projects/screen/ 
[tmux]: https://github.com/tmux/tmux/wiki
[byobu]: https://byobu.org/
