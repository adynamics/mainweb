+++
title = "Como migrar Debian a Devuan GNU/Linux"
date = 2019-04-29T21:47:24+02:00
draft = false

authors = ["jordila"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.

tags = ['systems', 'free software', 'linux', 'debian', 'devuan']
categories = ['main', 'free software']



# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 

#[image]
  # Caption (optional)
  #  caption = ""


[header]
image = "https://source.unsplash.com/collection/765594/1600x350"


  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++


# Intro

 Vamos a describir la receta de como realizar la migración a Devuan ASCII desde Debian Jessie o Stretch.
Actualmente la migración no es directa si [usas GNOME o network manager debido a algunos paquetes retenidos,
y cada migración respectiva tiene sus matices propios](https://devuan.org/os/documentation/dev1fanboy/migrate-to-ascii), pero esta receta debería funcionar, más allá de las  peculiaridades de ambos casos.

  ¿Debian vs Devuan?, técnicamente... ¿ que diferencia hay entre ambas distribuciones GNU+Linux (Debian vs Devuan) ? Ninguna. Una. Systemd .

  En cada sistema GNU+Linux existe un proceso que el Kernel arranca en primera instancia,
antes de todos los demás procesos. Es el proceso padre de todos aquellos procesos que  a su vez no tienen relación parental con otros procesos. otro Dicho proceso principal es conocido como pid1. Systemd es un pid1 recientemente desarrollado .

  Improbable lector@, si te apetece ahondar en el esta cuestión desde el aspecto teórico, [acá en  este artículo puedes hacerlo. De la mano y letra de el autor original de systemd](http://0pointer.de/blog/projects/systemd.html) . Por otra parte, en un otro [artículo, Litox nos habló de los «demonios y el sexo de los ángeles».](https://litox.entramado.net/2016/07/07/demonios-del-sistema-y-el-sexo-de-los-angeles/)

 Pasemos de la teoría a la práctica, pongamonos... ¿ manos a la obra ?

# Manos a la obra


Si hacemos un listado de los procesos que están corriendo en este momento en el servidor (Debian 9), observamos que 
entre ellos se encuentra systemd.

```bash
root@debian$ top
top - 14:39:12 up 25 days, 20:30,  1 user,  load average: 0.00, 0.00, 0.00
Tasks: 194 total,   1 running, 193 sleeping,   0 stopped,   0 zombie
%Cpu(s):  2.7 us,  9.0 sy,  0.0 ni, 88.4 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :   499072 total,    53452 free,   123600 used,   322020 buff/cache
KiB Swap:   370684 total,   339464 free,    31220 used.   323228 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                
27921 admin     20   0   44920   3452   2760 R  1.0  0.7   0:00.27 top                                                    
    1 root      20   0  204692   5756   4320 S  0.7  1.2   0:34.67 systemd                                                
  418 root      20   0   46484   3332   3020 S  0.3  0.7   0:07.79 systemd-logind                                         
  877 mysql     20   0  451316  47672   7292 S  0.3  9.6  37:04.65 mysqld                                                 
```

Devuan usa sysvinit por defecto. Así que, instalemoslo :

```bash
root@debian:~# apt-get install sysvinit-core
```

Se requiere un reinicio para realizar el cambio a sysvinit como proceso principal.

```
root@debian:~# reboot
```

Ahora podemos eliminar systemd sin más.

```bash
root@debian:~# apt-get purge systemd
```

Si ejecutamos de nuevo el comando para visualizar los procesos en ejecución en el servidor...

```bash
root@debian:~# top
Tasks: 191 total,   1 running, 190 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.3 us,  0.0 sy,  0.0 ni, 98.3 id,  1.3 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :   500096 total,   144792 free,   131692 used,   223612 buff/cache
KiB Swap:   370684 total,   370684 free,        0 used.   346304 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                
 2477 admin     20   0   44920   3484   2788 R  1.0  0.7   0:00.21 top                                                    
 2467 admin     20   0   69952   3704   2892 S  0.3  0.7   0:00.05 sshd                                                   
    1 root      20   0   15820   1852   1684 S  0.0  0.4   0:01.92 init                                                   
```

... podemos comprobar que sysvinit a sustituido a systemd como pid1.

Editamos el fichero sources.list de tal modo que podamos cambiar a los repositorios 
de paquetes software  de Devuan. 

```bash
root@debian:~# editor /etc/apt/sources.list
```

Añadimos lo servidores espejo (mirrors) con el nombre clave ascii y comentamos cualquier otra línea del mencionado fichero:

```bash
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
```


Actualizamos el índice de paquetes de tal modo que podamos instalar el archivo con el anillo de llaves (keyring)
de Devuan .

```bash
root@debian:~# apt-get update
```

Instalamos el anillo de llaves (keyring) de Devuan para que con él  puedan ser autenticados los paquetes de ahora en adelante.

```bash
root@debian:~# apt-get install devuan-keyring --allow-unauthenticated
```

Actualizamos el índice de paquetes de nuevo para que sean autenticados con el anillo de llaves (keyring)

```bash
root@debian:~# apt-get update
```

Finalmente podemos migrar a Devuan.

```bash
root@debian:~# apt-get dist-upgrade
```

# Tareas una vez realizada la migración 

Ahora, los componentes de systemd deberían ser eliminados del sistema.

```bash
root@devuan:~# apt-get purge systemd-shim
```

Si no usamos D-Bus o Xorg deberíamos poder eliminar libsystemd0.

```bash
root@devuan:~# apt-get purge libsystemd0
```

Purgamos cualquier paquete huérfano que haya quedado de la instalación Debian anterior.

```bash
root@devuan:~# apt-get autoremove --purge
```

Este es un buen momento para limpiar paquetes que hayan quedado obsoletos de nuestro sistema Debian.

```bash
root@devuan:~# apt-get autoclean
```

A disfrutar de Devuan GNU+Linux .
