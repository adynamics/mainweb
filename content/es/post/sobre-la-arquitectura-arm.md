+++
title = "Sobre la arquitectura ARM"
date = 2019-08-03T15:00:00+02:00
draft = false

authors = ["pietre"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['hack', 'arm', 'sistemas']
categories = ['main', 'hacks', 'arm', 'sistemas']

[header]
image = "https://source.unsplash.com/collection/765594/1600x350"

+++

Comenzaría diciendo que [ARM](https://es.wikipedia.org/wiki/Arquitectura_ARM) es lo mejor y ya está sería mas corto y no mentiría, pero obviamente no es tan simple, ARM es la arquitectura con más desarrollo tecnológico, supongo que debido al smart-all que en apenas 5 años se ha dado un enorme salto comparado a las demás. 

ARM es la arquitectura presente y futura, es el lugar a donde se dirige el consumo de dispositivos, recordemos que estos dispositivos son ordenadores. Quizás al principio no fuesen ordenadores, cuando ARM se popularizó fue con las [PDA](https://es.wikipedia.org/wiki/PDA) y primeros smartphones, esta época corresponde a un procesador ARM v5 204Mhz con 32Mb de [RAM](https://es.wikipedia.org/wiki/Memoria_de_acceso_aleatorio), limitados obviamente por el hardware, pero poco a poco se necesitó que estos procesadores calculasen rutas [GPS](https://es.wikipedia.org/wiki/GPS), gestionasen múltiples tareas al mismo tiempo y se creo el procesador ARM con coma flotante o hard float de ahí viene la arquitectura [*armhf*](https://stackoverflow.com/questions/37790029/what-is-difference-between-arm64-and-armhf). 

Cuando vemos un paquete para esta arquitectura se refiere a un tipo bastante concreto de procesador ARM de 32 bits, Cortex a7 o a9 y con cálculo de coma flotante.

*armhf* ha sido la reina de la arquitecturas hasta el cambio a 64 bits de ARM que hay que diferenciarlo del cambio de 32 a 64 bits de x86, el cambio de ARM ha sido brutal e incompatible, una transición que aun no esta ni mucho menos completada.

Afortunadamente de momento ARM no es compatible con ninguna versión utilizable de Microsoft Windows, quizás por eso me guste tanto :)

Normalmente para ejecutar [GNU/Linux](https://www.linux.org) en la arquitectura ARM necesitas un sofware que haga funciones de [BIOS](https://es.wikipedia.org/wiki/BIOS) y parte de bootloader. [GRUB](https://es.wikipedia.org/wiki/GNU_GRUB) esto lo hace muy flexible a la hora de elegir donde y cómo se ejecuta el sistema operativo, normalmente u-boot es el software elegido para iniciar GNU/Linux en la arquitectura ARM

Simplificado el proceso de arranque:

```
U-boot
|_kernel
|_dtb
|_initramfs
/rootfs
```

Como podéis ver es diferente a un inicio normal de un [PC](https://es.wikipedia.org/wiki/Computadora_personal).

Básicamente el u-boot se encarga de iniciar la [CPU](https://es.wikipedia.org/wiki/Unidad_central_de_procesamiento) y la RAM a la espera de una dirección válida para el [kernel](https://es.wikipedia.org/wiki/Núcleo_Linux), el árbol de dispositivos compilado [DTB](https://en.wikipedia.org/wiki/Device_tree) y el sistema de archivos llamado rootfs.

Si el u-boot tiene todo esto iniciaría el rootfs, es decir GNU/Linux (la variante que sea).

Para entender cómo funciona lo importante es conocer el lenguaje de descripción de hardware (DTB) sin esto no puedes hacer funcionar nada en el kernel actual de GNU/Linux. Estos conocimientos tienen que ser divulgados y asimilados por la Comunidad debido a su criticidad,  esto es información que permite iniciar un dispositivo ARM con GNU/Linux.

Por lo que podemos observar no entendemos muy bien como funciona GNU/Linux en ARM, y esto es preocupante sabiendo lo importante que es esta arquitectura para el ahora y el futuro.

# Armbian 

ARM es el nombre de la arquitectura y es difícil de explicar como se dividen en varias familias y estas a su vez en unas 160 subarquitecturas diferentes.

Por eso es difícil hablar de un sistema operativo "compatible ARM" debido a esta fragmentación inherente de la arquitectura, pero empiezan a surgir algunos proyectos coherentes que dan soporte a muchas de estas subarquitecturas ARM, esto se traduce en decenas de placas de desarrollo y algunos Smart TV.

Si te estás preguntando, si , GNU/Linux funciona en tablets y en Smart TV, y en otras muchas cosas, para eso estamos aquí, para que las veas.

Los sistemas subyacentes de este metadistro son [Debian](https://www.debian.org) y [Ubuntu](https://ubuntu.com), [Armbian](https://www.armbian.com) pone el u-boot, kernel y script de configuración de inicio, para como decía antes decenas de modelos de placas, esta es la primera distro universal para procesadores ARM de hay su importancia 

De todas estas cosas hablaremos en nuestros siguientes artículos.

Un saludo