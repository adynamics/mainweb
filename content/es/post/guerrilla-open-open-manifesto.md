+++
title = "Guerrilla Open Manifesto"
date = 2019-04-03T12:20:56+02:00
draft = false

authors = ["apardo"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['world', 'freedom', 'internet']
categories = ['main', 'internet']

[header]
image = "https://source.unsplash.com/collection/765594/1600x350"

+++

La información es poder. Pero como todo poder, hay quienes quieren mantenerlo para ellos mismos. La herencia científica y cultural del mundo entero, publicada durante siglos en libros y revistas, está siendo digitalizada y bloqueado su acceso por un puñado de empresas privadas. ¿Quieres leer los documentos que muestran los resultados más famosos de la ciencia? Deberás enviar grandes cantidades [de dinero] a editoriales como Reed Elsevier.

Están aquellos que luchan para cambiar esta situación. El Movimiento Open Access ha luchado valientemente para garantizar que los científicos no pierdan sus derechos de autor, sino que, en vez de eso, asegurar que su trabajo sea publicado en internet, bajo términos que permiten el acceso a todo el mundo. Pero, incluso en el mejor de los escenarios, esto solo se aplicará lo que publiquen en el futuro. Todo lo anterior a ahora se habrá perdido.

Eso es un precio muy alto a pagar. ¿Obligar a los investigadores a pagar para leer el trabajo de sus colegas? ¿Escanear bibliotecas enteras pero solo permitiendo que sea leído por el personal de Google? ¿Facilitar artículos científicos a aquellos que están en universidades de élite del Primer Mundo, pero no para los niños en el Cono Sur? Esto es escandaloso e inaceptable.

“Estoy de acuerdo”, dicen muchos, “Pero… ¿Qué podemos hacer? Las empresas que tienen los derechos de autor generan una gran cantidad de dinero cobrando por el acceso, y es perfectamente legal – no hay nada que podamos hacer para detenerlos.” Pero hay algo que podemos hacer, algo que ya se está haciendo: podemos contraatacar.

Aquellos con acceso a esos recursos – estudiantes, bibliotecarios, científicos – se os ha concedido un privilegio. Tiene la oportunidad de alimentarte en este banquete de conocimiento, mientras el resto del mundo se queda fuera. Pero no necesitáis  – de hecho, moralmente, no debéis – mantener este privilegio para ustedes mismos. Tenéis el deber de compartirlo con el mundo. Tenéis: intercambiar contraseñas con colegas de profesión, realizar las peticiones de descarga de amigos

Mientras tanto, los que se han quedado fuera no están de brazos cruzados. Os habéis estado infiltrando por los agujeros y saltando vallas, liberando la información bloqueada y compartiéndola con amigos.

Pero toda esta acción sucede en el oscuro y oculto mundo “underground”. Se le llama robo o piratería, como si compartir esta riqueza de conocimiento fuese el equivalente moral de saquear un barco y asesinar a su tripulación. Pero compartir no es inmoral – es un imperativo moral. Solo aquellos cegados por la codicia rehusarían dejar a un amigo hacerse una copia.

La grandes corporaciones, por supuesto, están cegadas por la codicia. Las leyes bajo las que operan así lo requieren – sus accionistas se rebelarían por ganar menos. Y los políticos a los que han sobornado, aprobando leyes que les conceden el poder exclusivo de decidir quien puede hacer copias.

No hay justicia en acatar leyes injustas. Es hora de salir a la luz y, en la gran tradición de la desobediencia civil, mostrar nuestra oposición a este robo privado de la cultura pública.

Debemos obtener la información, donde quiera que esté almacenada, hacer nuestras copias y compartirla con el mundo. Debemos recopilar material que no tenga derechos de autor y añadirlo al archivo. Debemos comprar bases de datos secretas y colgarlas en la Web. Debemos descargarnos publicaciones científicas y subirlas a las redes de intercambio de archivos. Debemos luchar por la Guerrilla Open Access.

Si somos los suficientes, alrededor del mundo, no solo vamos a mandar un fuerte mensaje de oposición a la privatización del conocimiento – vamos a convertirlo en algo del pasado. ¿Te nos unes?

Aaron Swartz

Julio 2008, Eremo, Italia