+++
# Slider widget.
widget = "slider"  # Do not modify this line!
active = true  # Activate this widget? true/false

# Order that this section will appear in.
weight = 1

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval = 5000

# Minimum slide height.
# Specify a height to ensure a consistent height for each slide.
height = "400px"

# Slides.
# Duplicate an `[[item]]` block to add more slides.
[[item]]
  title = "aDynamics"
  content = "We help you build any platform based on free software with UNIX or GNU/Linux."
  align = "left"  # Choose `center`, `left`, or `right`.

  # Overlay a color or image (optional).
  #   Deactivate an option by commenting out the line, prefixing it with `#`.
  overlay_color = "#272935"  # An HTML color value.
  #overlay_img = "headers/bubbles-wide.jpg"  # Image path relative to your `static/img/` folder.
  overlay_img = "ilya-pavlov-87438-unsplash.jpg"
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.

  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  #cta_label = "Contacta"
  #cta_url = "https://sourcethemes.com/academic/"
  #cta_icon_pack = "fas"
  #cta_icon = "graduation-cap"

[[item]]
  title = "aDynamics"
  content = "We have around 20 years of experience in the sector."
  align = "left"

  overlay_color = "#555"  # An HTML color value.
  overlay_img = "patryk-gradys-128898-unsplash.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.

[[item]]
  title = "aDynamics"
  content = "Get in touch with us and discover a new way of working."
  align = "left"

  overlay_color = "#333"  # An HTML color value.
  overlay_img = "alex-knight-199368-unsplash.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.
+++
