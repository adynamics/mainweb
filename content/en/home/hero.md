+++
# Hero widget.
widget = "hero"  # Do not modify this line!
active = false  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "aDynamics"

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
hero_media = "hero-academic.png"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  gradient_start = "#900e2c"
  gradient_end = "#2b94c3"
  
  # Background image.
  # image = ""  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.
[cta]
  url = "mailto:hola@aps.systems"
  label = "Contacta"
  icon_pack = "fas"
  icon = "pen-alt"
  
[cta_alt]
  url = "#network"
  label = "Conoce nuestra Red"
+++

**Te ayudamos a construir cualquier plataforma basada en software libre con UNIX o GNU/Linux**

Tenemos alrededor de 20 años de experiencia en el sector
