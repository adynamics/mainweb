+++
# Display name
name = "Radikal"

# Username (this should match the folder name)
authors = ["radikal"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Systems Administrator"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "My areas of interest are..."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "r4dikal@riseup.net"

# List (academic) interests or hobbies
interests = [
  "Systems",
  "Free Software",
  "HA",
  "IP Networks",
  "Sound Production"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Team"]

+++
