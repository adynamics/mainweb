+++
# Display name
name = "Pietre Linux"

# Username (this should match the folder name)
authors = ["pietre"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Kernel Developer"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "My areas of interest are mainly the kernel for ARM architecture."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "pietrelinux@riseup.net"

# List (academic) interests or hobbies
interests = [
  "ARM",
  "Linux Kernel",
  "Tablets",
  "Raspberry Pi",
  "Debian",
  "Training"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Team"]

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

+++

