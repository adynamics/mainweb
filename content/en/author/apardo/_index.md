+++
# Display name
name = "Antonio Pardo"

# Username (this should match the folder name)
authors = ["apardo"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Systems Architect"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = []

# Short bio (displayed in user profile at end of posts)
bio = "Areas of interest Unified Communications and Elixir language."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "apardo@aps.systems"

# List (academic) interests or hobbies
interests = [
  "Unified Communications",
  "Elixir",
  "Ruby",
  "Open Government"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Team"]

+++
