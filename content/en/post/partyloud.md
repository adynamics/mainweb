+++
title = "PartyLoud"
date = 2019-05-06T03:18:47+02:00
draft = false

authors = ["radikal"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['tools', 'shell', 'partyloud', 'fingerprinting']
categories = ['main', 'tools', 'shell']

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[header]
image = "https://source.unsplash.com/collection/765594/1600x300"
+++

[PartyLoud][link1] is a interesting project.
This is a tool to create fake traffic in order to mitigate tracking on local networks.
The idea behind this script is that if you make a lot of noise on the network (in the form of http requests) it will be less easy for an attacker to track your real navigation.

![PartyLoud][imagen1]

This project is based on [noisy.py][link2]
## Features
- Configurable urls list (partyloud.conf) and blocklist (badwords).
- Multi-threaded request engine (the number of threads is equal to the number of urls in partyloud.conf).
- Error recovery mechanism to protect engines from failure.
- Spoofed User Agent prevent from [*fingerprinting*][link3] (each engine has a different user agent)
- Dynamic UI.

## Install
To install and use PartyLoud, we will need **git** and **curl** 

In Debian and derivatives:

`sudo apt install git curl`

Clone the repository:

`git clone https://github.com/realtho/PartyLoud.git`

Navigate to the directory:

`cd PartyLoud`

Make the script executable:

`chmod +x partyloud.sh`

And run it:

`./partyloud.sh`

To end it, you can press the key **ENTER** or **CTRL + c**.
For more options, see help:

`./partyloud.sh -h`

[link1]: https://github.com/realtho/PartyLoud "PartyLoud on GitHub"
[link2]: https://github.com/1tayH/noisy "Noisy on GitHub"
[link3]: https://en.wikipedia.org/wiki/Fingerprint_(computing)
[imagen1]: https://camo.githubusercontent.com/bb1732abcaf93b1d8ba77dbadc6f11aa1ffd42c4/68747470733a2f2f692e696d6775722e636f6d2f636e31654546732e706e67
