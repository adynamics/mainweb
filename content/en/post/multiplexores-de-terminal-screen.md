+++
title = "Terminal Multiplexers Screen"
date = 2019-05-20T05:34:03+02:00
draft = false
authors = ["radikal"]
# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ['tools', 'shell', 'screen', 'multiplexer']
categories = ['main', 'tools', 'shell']

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
[header]
image = "https://source.unsplash.com/collection/765594/1600x300"
+++

# What allows you to make a multiplexer?

A **multiplexer** allows you:

- Use multiple shell windows from an ssh connection.
- Keep the shell active even if the network connection is broken.
- Disconnect and reconnect to a shell session from multiple locations.
- Run long-term processes without having a terminal running.

It has many uses. One of the best known multiplexers (which we will talk about today) is [screen][screen]. Next time we will talk about other multiplexers like [tmux][tmux] and [byobu][byobu].

# screen

Install screen in debian and derivatives:

`sudo apt install screen`

When executing screen it is very useful to give a **session name** to be able to recognize it and access it more easily.

`$ screen -S $session_name`

See active sessions:

`$ screen -ls`

Connect to a session:

`$ screen -r $session_name`

# Commands

Use "`ctrl`+`a`" as the main command followed by a key.

 Examples:

- "`ctrl`+`a`" `c`  - New windows
- "`ctrl`+`a`" `n`  - Next window
- "`ctrl`+`a`" `p`  - Previous window
- "`ctrl`+`a`" `d`  - Disconnect from the session (processes are still running)
- "`ctrl`+`a`" `S`  - Divide the window horizontally
- "`ctrl`+`a`" `|`  - Divide the window vertically

# Configuration

The global configuration file is in *`/etc/screenrc`* and the user in *`~/.screenrc`*

Example configuration:

~~~

# screenrc by radikal
# Turn off the annoying startup message
startup_message off

defscrollback 10000
hardcopy_append on
defutf8 on
term xterm
termcapinfo xterm*|Eterm|mlterm|rxvt 'hs:ts=\E]0;:fs=\007:ds=\E]0;screen\007'

# 8-16 Color Support
termcap  xterm AF=\E[3%dm
terminfo xterm AF=\E[3%p1%dm
termcap  xterm AB=\E[4%dm
terminfo xterm AB=\E[4%p1%dm

# 256 Color Support
terminfo xterm Co=256
termcap  xterm Co=256
termcap  xterm AF=\E[38;5;%dm
terminfo xterm AF=\E[38;5;%p1%dm
termcap  xterm AB=\E[48;5;%dm
terminfo xterm AB=\E[48;5;%p1%dm

# Set status line in window title, preview:
# [hostname]         0$ top  3-$ watch  (4*$ bash)        [load: 0,01 0,01 0,00] [mi?, 10/04/2019] [14:59:00]
hardstatus alwayslastline '%{= kG}[%{G}%H%? %1`%?%{g}] [%= %{= kw}%-w%{+b yk} %n*%t%?(%u)%? %{-}%+w %=%{g}] [%l] %{B}[%D, %d/%m/%Y] %{W}[%c:%s]%{g}'

# use F7-F8 to turn on|off the status bar off at the bottom of the screen
bindkey -k k7 hardstatus alwayslastline
bindkey -k k8 hardstatus alwaysignore

# Set left and right meta key mods ALT-nm.
bindkey "^[n" prev
bindkey "^[m" next

# Screens by default
~~~

For more information see the manual page: `man screen`

[screen]: https://savannah.gnu.org/projects/screen/ 
[tmux]: https://github.com/tmux/tmux/wiki
[byobu]: https://byobu.org/
